export interface Dados{
    nome?: String;
    email?: String;
    senha?: String;
    confirmaSenha?: String;
}

export class DadosCadastro implements Dados {
    constructor (
        public nome?: String,
        public email?: String,
        public senha?: String,
        public confirmaSenha?: String
    ){

    }
}