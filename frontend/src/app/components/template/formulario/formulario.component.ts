import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { DadosCadastro } from 'src/app/model/cadastro.model';
import { ServiceService } from 'src/components/template/service.service';

@Component({
  selector: 'app-formulario',
  templateUrl:'./formulario.component.html',
  styleUrls: ['./formulario.component.css']
})
export class FormularioComponent implements OnInit {
  validatorEmail = '^[a-zA-Z0-9_.+-]+@(?:(?:[a-zA-Z0-9-]+.)?[a-zA-Z]+.)?(hotmail|gmail|yahoo|outlook|live|terra).com(.br)?$';
  validatorSenha = '^(?=.*[A-Za-z])(?=.*\d)[A-Za-z\d]{8,}$'
  formularioDeUsuario: FormGroup;

  constructor(private fb: FormBuilder, private service: ServiceService) {
  }

  ngOnInit(): void {
    this.criarFormularioDeUsuario();
  }

  enviarDados() {
    const dadosFormulario = this.formularioDeUsuario.value;

    const usuario = new DadosCadastro (
      dadosFormulario.nome,
      dadosFormulario.email,
      dadosFormulario.senha,
      dadosFormulario.confirmarSenha
    );
    this.service.showMessage(`O usuário ${usuario.nome} foi cadastrado com sucesso.`) 

    this.formularioDeUsuario.reset();
  }

  criarFormularioDeUsuario(){
    this.formularioDeUsuario = this.fb.group({
      nome: ['', Validators.compose([Validators.required, Validators.minLength(6), Validators.maxLength(100)])],
      email: ['', Validators.pattern(this.validatorEmail)],
      senha: ['', Validators.required],
      confirmarSenha: ['',Validators.required]
    })
  }
  get nome() {
    return this.formularioDeUsuario.get('nome');
  }
  get email() {
    return this.formularioDeUsuario.get('email');
  }
  get senha() {
    return this.formularioDeUsuario.get('senha');
  }
  get confirmarSenha() {
    return this.formularioDeUsuario.get('confirmarSenha');
  }
}
