import { Injectable } from '@angular/core';
import { MatSnackBar } from '@angular/material/snack-bar'
@Injectable({
  providedIn: 'root'
})
export class ServiceService {

  constructor( private snackbar: MatSnackBar) { }

  showMessage(msg: string): void {
    this.snackbar.open(msg, 'X', {
      duration: 4000,
      horizontalPosition: 'right',
      verticalPosition: "top"
    })
  }
}
